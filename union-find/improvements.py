class WeightedQuickUnion:
    def __init__(self, n):
        self.size = n
        self.id = list(range(n))
        self.sz = [1] * n

    def _root(self, p):
        while p != self.id[p]:
            p = self.id[p]

        return p

    def is_connected(self, p, q):
        return self._root(p) == self._root(q)

    def union(self, p, q):
        """
        Maintain a size array and always point the
        root of smaller tree to point to root of
        the larger tree

        :param p:
        :param q:
        :return:
        """
        i, j = self._root(p), self._root(q)
        if i == j:
            return

        if self.sz[i] > self.sz[j]:
            self.id[j] = i
            self.sz[i] += self.sz[j]
        else:
            self.id[i] = j
            self.sz[j] += self.sz[i]


class PathCompression:
    def __init__(self, n):
        self.size = n
        self.id = list(range(n))
        self.sz = [1] * n

    def _root(self, p):
        """
        Two pass implementation, first find the root
        traverse again and set all the id's to root's

        :param p:
        :return:
        """
        temp = p
        while p != self.id[p]:
            p = self.id[p]

        while temp != self.id[temp]:
            temp, self.id[temp] = self.id[temp], p

        return p

    def _root_simple(self, p):
        """
        Single pass: make every node's point to its
        grandparent. Half the length

        :param p:
        :return:
        """
        while p != self.id[p]:
            self.id[p] = self.id[self.id[p]]
            p = self.id[p]

        return p

    def is_connected(self, p, q):
        return self._root(p) == self._root(q)

    def union(self, p, q):
        i, j = self._root(p), self._root(q)
        if i == j:
            return

        if self.sz[i] > self.sz[j]:
            self.id[j] = i
            self.sz[i] += self.sz[j]
        else:
            self.id[i] = j
            self.sz[j] += self.sz[i]

