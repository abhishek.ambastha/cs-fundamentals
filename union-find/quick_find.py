"""
In quick find, we maintain an array of id (length is number
of elements.

Two nodes are connected iff id[p] == id[q]

union is O(n)
find is O(1)
init is O(n)
"""


class QuickFind:
    def __init__(self, n):
        self.size = n
        self.id = list(range(n))

    def union(self, p, q):
        id_arr = self.id
        pid, qid = id_arr[p], id_arr[q]

        for i in range(self.size):
            if id_arr[i] == pid:
                id_arr[i] = qid

    def is_connected(self, p, q):
        return self.id[p] == self.id[q]

    def find(self, p):
        return self.id[p]

    def count(self):
        pass
