"""
In quick union, we keep an array of id (size n)

To merge components, set id of p's root to id of
q's root

To find root, id[id[id .... ]]]

union is O(1)
find is O(n)
"""


class QuickUnion:
    def __init__(self, n):
        self.size = n
        self.id = list(range(n))

    def _root(self, p):
        """
        The root node will have same the index and the id,
        keep going up, unless root is found

        :param p: node for which root is needed
        :return: root of node p
        """
        while p != self.id[p]:
            p = self.id[p]

        return p

    def is_connected(self, p, q):
        return self._root(p) == self._root(q)

    def union(self, p, q):
        """
        Union q and q nodes. Make the root of p
        to point to root of q

        :param p: Node
        :param q: Node
        :return: None
        """
        i, j = self._root(p), self._root(q)
        self.id[i] = j
