def edit_distance(self, word1: str, word2: str) -> int:
    """
    Basic Idea: f(s1, s2, i1, i2) is
                        1. if s1[i1] == s2[i2]                =>  f(s1, s2, i1+1, i2+1)
                        2. otherwise, min of  deletion in s2  => 1 + f(s1, s2, i1, i2+1)
                                    or insertion in s2        => 1 + f(s1, s2, i1+1, i2)
                                    or replacement in s2      => 1 + f(s1, s2, i1+1, i2+1)

    Base case,
    when either string is empty, result will be length of remaining non-empty string

    Complexity
    -----------
    Recursive version, each step of recursion has 3 options, so O(3^(n1+n2))
    With dp, there are m*n sub-problems, and time per sub-problem is O(1) [just +1],
             so time complexity becomes O(m*n)

    """
    s1, s2 = word1, word2
    n1, n2 = len(s1), len(s2)

    dp = [[-1 for _ in range(n2 + 1)] for _ in range(n1 + 1)]

    # first string is empty
    for i in range(n2 + 1):
        dp[0][i] = i

    # second string is empty
    for i in range(n1 + 1):
        dp[i][0] = i

    for i1 in range(1, n1 + 1):
        for i2 in range(1, n2 + 1):
            if s1[i1 - 1] == s2[i2 - 1]:
                dp[i1][i2] = dp[i1 - 1][i2 - 1]
                continue

            o1 = 1 + dp[i1 - 1][i2]
            o2 = 1 + dp[i1][i2 - 1]
            o3 = 1 + dp[i1 - 1][i2 - 1]
            dp[i1][i2] = min(o1, o2, o3)

    return dp[n1][n2]