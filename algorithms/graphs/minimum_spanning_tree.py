"""
Minimum Spanning Tree
---------------------

Definition:
-----------
For a connected graph G with positive edge weights, MST of G
is a subgraph T which is a tree which spans all the vertices


Greedy Algorithm:
-----------------
For any cut (partition of vertices into 2 sets), crossing edge
of minumum weight is in MST.

proof: if we include any other crossing edge, the total weight
will be larger, also there must be an edge since we need all
vertices to be connected (a tree).
"""
import heapq


class UF:
    def __init__(self, n):
        self.size = n
        self.id = list(range(n))
        self.sz = [1] * n

    def _root(self, p):
        """
        Two pass implementation, first find the root
        traverse again and set all the id's to root's

        :param p:
        :return:
        """
        temp = p
        while p != self.id[p]:
            p = self.id[p]

        while temp != self.id[temp]:
            temp, self.id[temp] = self.id[temp], p

        return p

    def _root_simple(self, p):
        """
        Single pass: make every node's point to its
        grandparent. Half the length

        :param p:
        :return:
        """
        while p != self.id[p]:
            self.id[p] = self.id[self.id[p]]
            p = self.id[p]

        return p

    def is_connected(self, p, q):
        return self._root(p) == self._root(q)

    def union(self, p, q):
        i, j = self._root(p), self._root(q)
        if i == j:
            return

        if self.sz[i] > self.sz[j]:
            self.id[j] = i
            self.sz[i] += self.sz[j]
        else:
            self.id[i] = j
            self.sz[j] += self.sz[i]


"""
Graph Representation
"""


class Edge:
    def __init__(self, v, w, weight):
        self.v = v
        self.w = w
        self.weight = w

    def either(self):
        return self.v

    def other(self, v):
        if not v == self.v:
            return self.v

        return self.w

    def compare(self, that):
        if self.weight < that.weight:
            return -1
        elif self.weight > that.weight:
            return 1
        else:
            return 0


class EdgeWeightedGraph:
    def __init__(self, n):
        self.size = n
        self.adj = [[] for _ in range(n)]

    def add_edge(self, edge):
        v = edge.either()
        w = edge.other(v)

        self.adj[v].append(edge)
        self.adj[w].append(edge)

    def adj(self, v):
        return self.adj

    def edges(self):
        return self.adj.values()


class KruskalMST:
    """
    Consider edges in ascending order of weight, and
    add edge to T unless it creates a cycle

    Time Complexity: E log(E)
    """

    def __init__(self, graph):
        self.graph = graph
        self.mst = None

    def calculate_mst(self):
        pq, mst = [], []
        for e in self.graph.edges():
            heapq.heappush(pq, (e.weight, e))

        uf = UF(self.graph.size)
        while pq and len(mst) < self.graph.size - 1:
            edge = heapq.heappop(pq)
            v = edge.either()
            w = edge.other(v)

            if not uf.is_connected(v, w):
                uf.union(v, w)
                mst.append(edge)

        self.mst = mst

    def edges(self):
        return self.mst


class LazyPrimMST:
    """
    Start with vertex 0 and greedly grow the tree

    Add to T the min weight edge with exactly one
    endpoint in T

    Time Complexity : E log(E)
    Space Complexity: E
    """
    def __init__(self, graph):
        self.graph = graph
        self.mst = None
        self.pq = []
        self.marked = [False] * len(graph.size)

    def calculate_mst(self):
        pq, mst = self.pq, []
        marked = self.marked

        self.visit(0)

        while pq and len(mst) < self.graph.size:
            edge = heapq.heappop(pq)
            v = edge.either()
            w = edge.other(v)

            if marked[v] and marked[w]:
                continue
            mst.append(edge)

            if not marked[v]:
                self.visit(v)

            if not marked[w]:
                self.visit(w)

    def visit(self, v):
        self.marked[v] = True
        for e in self.graph.adj[v]:
            if self.marked[e.other(v)]:
                continue

            self.pq.append((e.weight, e))

    def edges(self):
        return self.mst


