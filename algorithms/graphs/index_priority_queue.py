class Element:
    def __init__(self, val, index):
        self.val = val
        self.index = index

    def __lt__(self, other):
        return self.val < other.val

    def __gt__(self, other):
        return self.val > other.val

    def __eq__(self, other):
        return self.val == other.val

    def __repr__(self):
        return str(self.val)


class IndexedPriorityQueue:
    def __init__(self):
        self.data = []
        self.keys = {}
        self.pos = {}

    @staticmethod
    def left(i):
        return 2*i + 1

    @staticmethod
    def right(i):
        return 2*i + 2

    @staticmethod
    def parent(i):
        return i // 2

    @property
    def size(self):
        return len(self.data)

    def swim(self, i):
        while i > 0 and self.data[i] < self.data[self.parent(i)]:
            self.swap(i, self.parent(i))
            i = self.parent(i)

    def sink(self, i):
        while self.left(i) <= self.size:
            l, r = self.left(i), self.right(i)

            smallest = i
            if l < self.size and self.data[l] < self.data[smallest]:
                smallest = l

            if r < self.size and self.data[r] < self.data[smallest]:
                smallest = r

            if smallest != i:
                self.swap(smallest, i)
                i = smallest
            else:
                break

    def swap(self, i, j):
        self.pos[self.data[j].index], self.pos[self.data[i].index] = \
            self.pos[self.data[i].index], self.pos[self.data[j].index]

        self.data[i], self.data[j] = self.data[j], self.data[i]

    def insert(self, i, key):
        if i in self.keys:
            raise AttributeError('Index already exists!')

        self.keys[i] = Element(key, i)

        self.data.append(self.keys[i])
        self.pos[i] = len(self.data) - 1
        self.swim(len(self.data)-1)

    def decrease_key(self, i, new_key):
        pos = self.pos[i]
        if self.data[pos].val < new_key:
            raise AttributeError('New key is greater than existing!')

        self.data[pos].val = new_key
        self.swim(pos)

    def delete_min(self):
        val = self.data[0]
        self.data[0], self.data[-1] = self.data[-1], self.data[0]
        self.data = self.data[0:-1]
        self.sink(0)
        return val


def test():
    hh = IndexedPriorityQueue()
    vals = [9, 4, 5, 7, 8, 8, 11]
    for i, v in enumerate(vals):
        print(f'{v} with index {i}')
        hh.insert(i, v)

    import heapq
    heapq.heapify(vals)

    # for _ in range(len(vals)):
    #     print(heapq.heappop(vals), hh.delete_min())

    hh.decrease_key(2, 2)
    hh.decrease_key(5, 1)
    for _ in range(len(vals)):
        ele = hh.delete_min()
        print(f'element at {ele.index} is {ele.val}')


if __name__ == '__main__':
    test()