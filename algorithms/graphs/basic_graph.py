from collections import defaultdict, deque


class Graph:
    def __init__(self, num_vertices, directed=True):
        self.num_vertices = num_vertices
        self.adj = defaultdict(list)
        self.directed = directed

    def add_edge(self, v1, v2):
        self.adj[v1].append(v2)
        if not self.directed:
            self.adj[v2].append(v1)

    def bfs(self, start):
        # start exploring the starting node
        print('start exploring:', start)

        q = deque([start])
        visited = [False] * self.num_vertices
        visited[start] = True

        while q:
            node = q.popleft()
            for curr in self.adj[node]:
                if not visited[curr]:
                    visited[curr] = True
                    q.append(curr)
                    print('start exploring:', curr)

            print('finished exploring:', node)

    def dfs(self, start):
        visited = [False] * self.num_vertices
        self._dfs(start, visited)

    def _dfs(self, node, visited):
        visited[node] = True

        print('start exploring:', node)
        for curr in self.adj[node]:
            if not visited[curr]:
                self._dfs(curr, visited)
        print('finished exploring:', node)

    def has_cycle(self):
        color = [0] * self.num_vertices

        for i in range(self.num_vertices):
            if color[i] == 0 and self._cycle(i, color):
                return True

        return False

    def _cycle(self, node, color):
        color[node] = 1
        for n in self.adj[node]:
            if color[n] == 0 and self._cycle(n, color):
                return True

            elif color[n] == 1:
                return True

        color[node] = 2
        return False


def main():
    graph = Graph(6)
    graph.add_edge(0, 1)
    graph.add_edge(0, 2)
    graph.add_edge(0, 3)
    graph.add_edge(1, 2)
    graph.add_edge(1, 3)
    graph.add_edge(3, 4)
    graph.add_edge(4, 5)

    g = Graph(4)
    g.add_edge(0, 1)
    g.add_edge(0, 2)
    g.add_edge(1, 2)
    g.add_edge(2, 0)
    g.add_edge(2, 3)
    g.add_edge(3, 3)

    print('Performing BFS ...\n')
    graph.bfs(0)

    print('\n\nPerforming DFS ...\n')
    graph.dfs(0)

    print(graph.has_cycle())
    print(g.has_cycle())


if __name__ == '__main__':
    main()
