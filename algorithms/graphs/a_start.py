#!/usr/bin/env python

"""
A* algorithm: Find shortest path

1. Generate a list of all possible next steps
toward goal from current position

2. Store children in PQ based on distance to
goal.

3. Select closest child and REPEAT until goal
reached or no more children

"""
from queue import PriorityQueue


class State(object):
    def __init__(self, value, parent, start=0, goal=0):
        self.children = []
        self.parent = parent
        self.value = value
        self.dist = 0

        if parent:
            self.path = list(parent.path)
            self.path.append(value)
            self.start = parent.start
            self.goal = parent.goal
        else:
            self.path = [value]
            self.start = start
            self.goal = goal

    def get_dist(self):
        pass

    def create_children(self):
        pass


class StateString(State):
    def __init__(self, value, parent, start=0, goal=0):
        super(StateString, self).\
            __init__(value, parent, start, goal)
        self.dist = self.get_dist()

    def get_dist(self):
        if self.value == self.goal:
            return 0
        dist = 0
        for i in range(len(self.goal)):
            letter = self.goal[i]
            dist += abs(i - self.value.index(letter))
        return dist

    def create_children(self):
        if self.children:
            return
        for i in range(len(self.goal)-1):
            val = self.value
            val = val[:i] + val[i+1] + val[i] + val[i+2:] # swap letters
            child = StateString(val, self)
            self.children.append(child)


class AstarSolver:
    def __init__(self, start, goal):
        self.path = []
        self.visitedQueue = []
        self.priorityQueue = PriorityQueue()
        self.start = start
        self.goal = goal

    def solve(self):
        start_state = StateString(self.start, 0, self.start, self.goal)

        count = 0
        self.priorityQueue.put((0, count, start_state))

        while not self.path and self.priorityQueue.qsize():
            closestChild = self.priorityQueue.get()[2]
            closestChild.create_children()
            self.visitedQueue.append(closestChild.value)

            for child in closestChild.children:
                if child.value not in self.visitedQueue:
                    count += 1
                    if not child.dist:
                        self.path = child.path
                        break

                    self.priorityQueue.put((child.dist, count, child))

            if not self.path:
                print(f'Goal of {self.goal} is not possible')

            return self.path


if __name__ == '__main__':
    start1 = 'cdabfe'
    goal1 = 'abcdef'

    a = AstarSolver(start1, goal1)
    a.solve()

    for i in range(len(a.path)):
        print(i, a.path[i])




