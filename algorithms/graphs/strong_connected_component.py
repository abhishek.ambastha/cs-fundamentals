"""
Strongly connected components
----------------------------------------------------------------------------------

To compute the strongly connected components
1. Call DFS on the directed graph, and produce topological ordering of vertices
2. Reverse the graph. (Transpose)
3. Call DFS on the reversed graph, in the topological order generated in 1. The
trees produced are the strongly connected components.

"""

from collections import defaultdict


class Graph:
    def __init__(self, vertices):
        self.vertices = vertices
        self.adj = defaultdict(list)

    def add_edge(self, u, v):
        self.adj[u].append(v)

    def transpose(self):
        reversed_graph = Graph(self.vertices)

        for i in range(self.vertices):
            for j in self.adj[i]:
                reversed_graph.add_edge(j, i)

        return reversed_graph

    def topological_sort(self):
        visited = [False] * self.vertices
        result = []

        for i in range(self.vertices):
            if not visited[i]:
                self._dfs(i, visited, result)

        return list(reversed(result))

    def _dfs(self, start, visited, result):
        if visited[start]:
            return

        visited[start] = True
        for i in self.adj[start]:
            if not visited[i]:
                self._dfs(i, visited, result)

        result.append(start)

    def connected_component(self):
        topological_order = self.topological_sort()
        transposed_graph = self.transpose()

        ans = []
        visited = [False] * self.vertices
        for node in topological_order:
            result = []

            transposed_graph._dfs(node, visited, result)
            if result:
                ans.append(result)

        return ans


def test():
    graph = Graph(5)
    """
    1 2 => 0, 1
    2 3 => 1, 2
    3 4 => 2, 3
    4 5 => 3, 4
    4 2 => 3, 1
    """
    graph.add_edge(0, 1)
    graph.add_edge(1, 2)
    graph.add_edge(2, 3)
    graph.add_edge(3, 4)
    graph.add_edge(3, 1)

    scc = graph.connected_component()
    print(scc)


if __name__ == '__main__':
    test()
