"""
Given a string S and a string T, find the minimum window in S which will contain all
the characters in T in complexity O(n).

Example:

Input: S = "ADOBECODEBANC", T = "ABC"
Output: "BANC"
Note:

If there is no such window in S that covers all characters in T, return the empty string "".
If there is such window, you are guaranteed that there will always be only one unique minimum window in S.


"""
from collections import Counter


def min_window(s, t):
    char_freq = Counter(t)   # freq counter
    total = len(t)

    start, end = 0, 0
    ans, min_so_far = '', float('inf')

    while end < len(s):
        endchar = s[end]

        if endchar in char_freq:
            char_freq[endchar] -= 1
            if char_freq[endchar] >= 0:
                total -= 1

        end += 1

        while total == 0:
            if end - start < min_so_far:
                ans = s[start:end]
                min_so_far = end - start

            if s[start] in char_freq:
                char_freq[s[start]] += 1
                if char_freq[s[start]] > 0:
                    total += 1

            start += 1

    return ans