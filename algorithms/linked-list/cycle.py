def find_cycle(head):
    """
    Basic Idea: 2 pointers slow and fast, slow moves one step at a time
    and fast moves two at a time. If they both meet, there must be a cycle

    Now to find first node of cycle.

    |<-------- d ------->|<-- k --->|
    n1 - n2 - n3 - n4 - n5 - n6 - n7
                         |        |
                        n10 - n9 - n8

    let's assume both slow and fast meet at n7, d+k from head.
    if slow has moved N steps,
        N = d + k + c * i , where i is an integer multiple  EQ1

    then fast has moved twice as much, that is
       2N = d + k + c * j , where j is an integer multiple  EQ2

    => 2(d + k + c*i) = d + k + c * j
    => d + k = c * (j - 2* i)
    or d + k = c * p  , where p is an integer multiple
    or d = c * p - k                                        EQ3

    which means, after slow and fast meet at distance d + k from head,
    if one pointer moves d steps from head, and simultaneously
    other pointer moves d steps from the meeting point, the other pointer
    will be k short of complete cycle (from EQ3), which is the head of cycle.
    """

    slow, fast = head, head
    while fast and fast.next and fast.next.next:
        slow = slow.next
        fast = fast.next.next

        if slow == fast:
            slow = head
            while slow is not fast:
                slow, fast = slow.next, fast.next
            # meeting point after d steps, fast pointer is at head of cycle
            return slow

    # no cycle
    return None
