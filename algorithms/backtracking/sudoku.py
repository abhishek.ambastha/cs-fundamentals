class Sudoku:
    """ This class aims to provide solution for sudoku puzzles. The class
    is initialized with a sample sudoku puzzle, which is a 9x9 matrix of
    numbers. This can be reinitialized to some other matrix, using the
    Soduku.grid variable.

    The approach to solve this puzzle is back-tracking, where each and every
    possibility (1,9) are tried for every blank (initialized to 0). If a
    solution is found, it is printed to the console.
    """
    def __init__(self):
        self.grid = [[0, 6, 0, 1, 0, 4, 0, 5, 0],
                     [0, 0, 8, 3, 0, 5, 6, 0, 0],
                     [2, 0, 0, 0, 0, 0, 0, 0, 1],
                     [8, 0, 0, 4, 0, 7, 0, 0, 6],
                     [0, 0, 6, 0, 0, 0, 3, 0, 0],
                     [7, 0, 0, 9, 0, 1, 0, 0, 4],
                     [5, 0, 0, 0, 0, 0, 0, 0, 2],
                     [0, 0, 7, 2, 0, 6, 9, 0, 0],
                     [0, 4, 0, 5, 0, 8, 0, 7, 0]]
        self.solution = self.grid

    def display(self):
        """ Display the currently initialized grid on the console.

        :return: None
        """
        for r in range(9):
            for c in range(9):
                print(self.grid[r][c], '\t', end='')
            print('\n')

    def solve(self, row, col):
        """ Solve the sudoku puzzle with currently initialized grid.

        :param row: next row number where a value should be filled
        :param col: next col number where a value should be filled
        :return: None
        """
        if row == 9:
            self.display()
            self.solution = self.grid
            return

        if self.grid[row][col] != 0:
            new_row, new_col = self._advance(row, col)
            self.solve(new_row, new_col)
        else:
            for k in range(1,10):
                if not self._is_valid_candidate(row, col, k):
                    continue

                self.grid[row][col] = k

                new_row, new_col = self._advance(row, col)

                self.solve(new_row, new_col)

            self.grid[row][col] = 0

    @staticmethod
    def _advance(row, col):
        """ Util method to get the next row and column index.

        :param row: current row index
        :param col: current column index
        :return: Next row, col indices
        """
        if col == 8:
            return row + 1, 0
        else:
            return row, col + 1

    def _is_valid_candidate(self, row, col, k):
        """ Validate if a given value (k) can be filled at row, col.

        :param row: current row index
        :param col: current column index
        :param k: number 1-9, which has to be checked for
        :return: True if k can be used in (row, col), False otherwise
        """
        # check row violation
        for c in range(9):
            if c != col and self.grid[row][c] == k:
                return False

        # check column violation
        for r in range(9):
            if r != row and self.grid[r][col] == k:
                return False

        # check violation in box
        r_box = row // 3
        c_box = col // 3
        for r in range(3*r_box, 3*r_box + 3):
            for c in range(3*c_box, 3*c_box + 3):
                if r != row and c != col and self.grid[r][c] == k:
                    return False

        return True


def main():
    """
    Driver code for a sample soduku puzzle.
    :return: None
    """
    instance = Sudoku()
    instance.display()
    print('\n***********************************\n')
    instance.solve(0, 0)


if __name__ == '__main__':
    main()
