"""
This module implements the solution of the popular n-queens problem
stated below.

Given a chess-board of N x N dimensions, place N queens on the chess-
board such that they are all at non-attacking positions. That is, no
queen can attack other queen on the board.

Solution:
---------
The data-structure we will use to represent the board will be simply
an array of N elements [0, N), where the index i indicates the row
and the corresponding value at i indicates the column of each queen
placed on the board.

Since the array would contain numbers from 0 - N-1, the horizontal
and vertical constraints will be automatically satisfied given our
choice of data-structure. We would however need to take care of the
diagonal directions explicitly.

There are 2N-1 diagonals in both directions. Let r, c be the row and
column positions respectively. Then

One set of diagonals will lie on  r + c = 0,
while the other will be  on  r - c = 0.

To denote these lines we can use an array of len 2N - 1
diag_pos = [0, 2N-1)
diag_neg = [-(N-1), (N-1)], we can use offset of N-1
"""


def solve_chessboard(solution, N, free_col, free_diag_pos, free_diag_neg):
    if len(solution) == N:
        print_board(solution)
        return True

    curr_idx = len(solution)
    for i in range(N):
        if not (free_col[i] and free_diag_pos[i + curr_idx] and free_diag_neg[curr_idx - i + N - 1]):
            continue

        solution = solution + [i]
        free_col[i] = False
        free_diag_neg[curr_idx - i + N - 1] = False
        free_diag_pos[curr_idx + i] = False

        solved = solve_chessboard(solution, N, free_col, free_diag_pos, free_diag_neg)

        del solution[-1]
        free_col[i] = True
        free_diag_neg[curr_idx - i + N - 1] = True
        free_diag_pos[curr_idx + i] = True

        # get only one solution
        if solved:
            break


def print_board(sol):
    print(sol)


def main():
    n = int(input('Enter chessboard dimension >= 4. Press enter for default [4]: '))
    solve_chessboard([], n, [True]*n, [True]*(2*n-1), [True]*(2*n-1))


if __name__ == '__main__':
    main()
