def generate_permutation(solution, available, elements):
    """
    Generate all possible arrangements of elements (elements array)

    :param solution:  array containing the arrangement
    :param available: boolean array to keep track of used elements
    :param elements:  array containing actual elements
    :return: None
    """
    if len(solution) == len(elements):
        print_permutation(solution)
        return

    for i, element in enumerate(elements):
        if not available[i]:
            continue

        solution = solution + [element]
        available[i] = False

        generate_permutation(solution, available, elements)

        available[i] = True
        del solution[-1]


def print_permutation(solution):
    """
    Print the solution array

    :param solution: array containing elements to be printed
    :return: None
    """
    print('{', ', '.join(solution), '}')


def main():
    """
    Driver code to test permutation

    :return: None
    """
    arr = input('Enter elements to permute. Enter for default [a,b,c]: ')
    if not arr:
        arr = 'a, b, c'

    arr = arr.split(',')
    arr = list(map(str.strip, arr))

    generate_permutation([], [True]*len(arr), arr)


if __name__ == '__main__':
    main()
