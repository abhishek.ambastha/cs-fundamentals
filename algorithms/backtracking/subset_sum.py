def subset_sum(solution, i, curr_sum, elements, target_sum):
    """
    Given a set of numbers, and a target sum, print all possible
    subsets S, such that the sum of all elements in S is equal to
    the target sum

    :param solution:    binary array which indicates if element at
                        i is included in solution
    :param i:           the position index
    :param curr_sum:    the running sum
    :param elements:    array containing actual elements
    :param target_sum:  the target sum
    :return:  None
    """
    if curr_sum == target_sum:
        print_subset(solution, elements)
        return

    if i >= len(elements):
        return

    for k in range(2):
        if curr_sum + k * elements[i] > target_sum:
            continue

        solution[i] = k
        curr_sum = curr_sum + k * elements[i]
        subset_sum(solution, i+1, curr_sum, elements, target_sum)
        solution[i] -= k
        curr_sum = curr_sum - k*elements[i]


def print_subset(solution, elements):
    """
    Print the solution

    :param solution: Array containing {0, 1} indicating if element has to be included
    :param elements: Original array containing the actual elements
    :return: None
    """
    print('{', end='')
    for i in range(len(elements)):
        if solution[i]:
            print(elements[i], ',', end='')
    print('}')


def main():
    arr = input('Enter the collection of numbers: ')
    if not arr:
        arr = '1, 2, 3, 5, 6, 7, 9'
    arr = arr.split(',')
    arr = list(map(int, arr))

    target = input('Enter target sum: ')
    if not target:
        target = '13'
    target = int(target)

    print('Cacluating on :', arr, target)
    subset_sum([0]*len(arr), 0, 0, arr, target)


if __name__ == '__main__':
    main()
