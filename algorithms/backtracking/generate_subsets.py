def generate_subsets(solution, elements):
    """
    Generate all possible combinations of elements in array elements

    :param solution: array containing the solution elements
    :param elements: original array which contains actual elements
    :return: None
    """

    if len(solution) == len(elements):
        print_solution(solution, elements)
        return

    for k in range(2):
        solution = solution + [k]

        generate_subsets(solution, elements)

        del solution[-1]


def print_solution(solution, elements):
    """
    Print the combination array

    :param solution: Array containing {0, 1} indicating if element has to be included
    :param elements: Original array containing the actual elements
    :return: None
    """
    print('{', end='')
    for i in range(len(elements)):
        if solution[i]:
            print(elements[i], ',', end='')
    print('}')


def main():
    arr = input('Enter elements of array: [a, b, c]: ')
    if not arr:
        arr = 'a,b,c'

    arr = arr.split(',')

    generate_subsets([], arr)


if __name__ == '__main__':
    main()
