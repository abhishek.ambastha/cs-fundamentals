def mergesort(arr):
    """
    Basic Idea:
        1. Divide array into two halves
        2. Recursively sort each half
        3. Merge two halves

    Note: do not initialize auxiliary array in recursive step for efficiency

    Complexity Analysis
    -------------------
    At most N lg N compares
    C(N) <= 2 * C(N/2) + N

    """
    sort(arr, list(arr), 0, len(arr)-1)


def sort(arr, aux, lo, high):
    if high <= lo:
        return
    mid = lo + (high-lo)//2
    sort(arr, aux, lo, mid)
    sort(arr, aux, mid+1, high)
    if arr[mid] < arr[mid+1]:
        # no need to merge
        return
    merge(arr, aux, lo, mid, high)


def sort_iter(arr):
    aux = list(arr)
    N = len(arr)
    sz = 1
    while sz < N:
        lo = 0
        while lo < N - sz:
            merge(arr, aux, lo, lo+sz-1, min(lo+sz+sz-1, N-1))
            lo += sz + sz
        sz = sz + sz


def merge(arr, aux, lo, mid, high):
    for i in range(lo, high+1):
        aux[i] = arr[i]

    i, j = lo, mid + 1
    for k in range(lo, high+1):
        if i > mid:
            arr[k], j = aux[j], j + 1
        elif j > high:
            arr[k], i = aux[i], i + 1
        elif aux[i] < aux[j]:
            arr[k], i = aux[i], i + 1
        else:
            arr[k], j = aux[j], j + 1


if __name__ == '__main__':
    inp = [5, 3, 1, 9, 12]
    mergesort(inp)
    print(inp)
