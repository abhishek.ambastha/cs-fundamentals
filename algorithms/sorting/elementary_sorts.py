"""
The file contains sorting algorithm as taught by the princeton course
on algorithm by Robert Sedgewick & Kevin Wayne. Solely intended for
educational purposes.

"""
import random


def selection_sort(arr):
    """
    Basic Idea: In iteration i , find index min of the smallest remaining entry.
                Swap curr with minimum.

    Invariants: 1. arr[:i+1] is sorted.
                2. arr[:i+1] <= arr[i+1:]

    Complexity Analysis:
    --------------------
    Always O(n^2), even if input is sorted
    Minimum exchange - Linear Exchanges


    param arr: array of numbers in random order
    :return: sorted array in ascending order
    """
    for i in range(len(arr)):
        min_idx = i
        for j in range(i, len(arr)):
            if arr[j] < arr[min_idx]:
                min_idx = j
        arr[i], arr[min_idx] = arr[min_idx], arr[i]

    return arr


def insertion_sort(arr):
    """
    Basic Idea: In iteration i, swap a[i] with each larger entry to its left.
    Invariants: 1. arr[:i+1] is sorted
                2. arr[i+1:] - unclassified or unseen


    Complexity Analysis:
    --------------------
    To sort a randomly-ordered array with distinct keys, insertion sort
    uses ~ (1/4) N^2 compares and ~ (1/4) N^2 exchanges on average.

    Best case. If the array is in ascending order, insertion sort makes
    N-1 compares and 0 exchanges.

    Worst case. If the array is in descending order (and no duplicates),
    insertion sort makes ~ (1/2) N^2 compares and ~ (1/2) N^2 exchanges.

    Extra Info
    ----------
    For partially-sorted arrays, insertion sort runs in linear time.
    Pf. Number of exchanges equals the number of inversions.

    An array is partially sorted if the number of inversions is ≤ c N
    An inversion is a pair of keys that are out of order.



    param arr: array of numbers in random order
    :return: sorted array in ascending order
    """
    for i in range(len(arr)):
        for j in range(i, 0, -1):
            if arr[j] < arr[j - 1]:
                arr[j - 1], arr[j] = arr[j], arr[j - 1]
            else:
                break

    return arr


def shell_sort(arr):
    """
    Basic Idea: Move entries more than one position at a time by h-sorting the array.
    h-sort array for decreasing sequence of values of h.
    Or, insertion sort, with stride length h.

                    L E E A M H L E P S O L T S X R
                    L-------M-------P-------T
                      E-------H-------S-------S
                        E-------L-------O-------X
                          A-------E-------L-------R


    Complexity Analysis
    --------------------
    Proposition. The worst-case number of compares used by shellsort with
    the 3x+1 increments is O(N^3/2 )

    Property. Number of compares used by shell-sort with the 3x+1 increments
    is at most by a small multiple of N times the # of increments used.

            |   N    | compares | N^1.289 | 2.5 N lg N |
            |--------|----------|---------|------------|
            | 5,000  |       93 |      58 |        106 |
            | 10,000 |      209 |     143 |        230 |
            | 20,000 |      467 |     349 |        495 |
            | 40,000 |     1022 |     855 |       1059 |
            | 80,00  |     2266 |    2089 |       2257 |


    param arr: array of numbers in random order
    :return: sorted array in ascending order
    """
    h = 1
    while h < len(arr) // 3:
        h = 3 * h + 1

    while h >= 1:
        for i in range(h, len(arr), 1):
            for j in range(i, h - 1, -1 * h):
                if arr[j] < arr[j - h]:
                    arr[j], arr[j - h] = arr[j - h], arr[j]
                else:
                    break
        h = h // 3

    return arr


def shuffle(arr):
    """
    Basic Idea: In iteration i , pick integer r between [0, i] uniformly at random.
    and swap a[i] and a[r].

    Common Bugs: r is picked between 0, N-1 instead of [0, i] or [i, N-1].

    Complexity Analysis:
    --------------------
    Linear time. O(n)

    :param arr: input array of numbers
    :return: random uniform permutation of the input array
    """
    for i in range(len(arr)):
        r = random.randint(0, i)
        arr[i], arr[r] = arr[r], arr[i]

    return arr


def test():
    arr = [9, 5, 7, 2, 3, 4]
    # selection_sort(arr)
    # insertion_sort(arr)
    shell_sort(arr)
    print(arr)
    shuffle(arr)
    print(arr)


if __name__ == '__main__':
    test()
