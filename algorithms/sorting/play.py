import random


def shuffle(arr):
    for i in range(len(arr)):
        r = random.randint(i, len(arr)-1)
        arr[r], arr[i] = arr[i], arr[r]


def dutch_partition(arr, lo, hi):
    pivot = arr[lo]
    # print(f'Pivot around {pivot}')
    lt, i, gt = lo, lo, hi

    while i <= gt:
        if arr[i] < pivot:
            arr[i], arr[lt] = arr[lt], arr[i]
            i += 1
            lt += 1
        elif arr[i] > pivot:
            arr[gt], arr[i] = arr[i], arr[gt]
            gt -= 1
        else:
            i += 1

    return lt


def partition(arr, lo, hi):
    i, j = lo + 1, hi

    while i <= j:
        while i < hi and arr[i] < arr[lo]:
            i = i + 1

        if arr[i] == arr[lo]:
            i = i + 1

        while j > lo and arr[j] > arr[lo]:
            j = j - 1

        if arr[j] == arr[lo]:
            j = j - 1

        if i >= j:
            break

        arr[i], arr[j] = arr[j], arr[i]

    arr[lo], arr[j] = arr[j], arr[lo]
    return j


def quicksort_helper(arr, lo, hi):
    if hi <= lo:
        return

    j = dutch_partition(arr, lo, hi)
    quicksort_helper(arr, lo, j-1)
    quicksort_helper(arr, j+1, hi)


def quicksort(arr):
    shuffle(arr)
    quicksort_helper(arr, 0, len(arr)-1)


def find_kth(arr, k):
    lo, hi = 0, len(arr) - 1

    while lo <= hi:
        j = dutch_partition(arr, lo, hi)
        if j < k:
            lo = j+1
        elif j > k:
            hi = j-1
        else:
            return arr[j]

    return -1


def test():
    ar = list(range(20))
    shuffle(ar)
    print(ar)

    i = dutch_partition(ar, 0, len(ar)-1)
    print(i, ar)

    quicksort(ar)
    print(ar)

    shuffle(ar)
    print(ar)

    r = random.randint(0, len(ar))
    print(r, find_kth(ar, r))


if __name__ == '__main__':
    test()
