"""
Graham Scan Algorithm
---------------------

n = # of input points
h = # of points on the hull

Complexity O(n log(n))
Gift Wrapping O(nh)

Steps:
------

1. Find point P with lowest y-coordinate. (if duplicates, choose
one with lowest x-coordinate

2. Sort the remaining points in order of increasing polar angle
from P (if duplicates, order by increasing distance from P)

3. Initialize the hull with the anchor point P and the first element
in the sorted list.

4. Iterate over each point in the sorted list and determine if
traversing to ut from the prior two points in the hull constitutes
making CW or CCW rotation. If CW, backtrack (delete points on hull)
until adding this point constitutes a CCW rotation.

"""

from matplotlib import pyplot as plt
from random import randint
from math import atan2


def graham_scan(points, show_progress=False):
    min_idx = None
    for i, (x,y) in enumerate(points):
        if not min_idx or y < points[min_idx][1]:
            min_idx = i

        if y == points[min_idx][1] and x < points[min_idx][0]:
            min_idx = i

    anchor = points[min_idx]
    sorted_pts = quicksort(points, anchor)
    # del sorted_pts[sorted_pts.index(anchor)]

    hull = [anchor, sorted_pts[1]]
    for s in sorted_pts[2:]:
        print('ok', s)
        while det(hull[-2], hull[-1], s) <= 0:
            del hull[-1]
            if len(hull) < 2:
                break

        hull.append(s)
        if show_progress:
            scatter_plot(points, hull)

    return anchor, hull


def quicksort(a, anchor):
    if len(a) <= 1:
        return a
    smaller, equal, larger = [], [], []
    piv_angle = polar_angle(a[randint(0, len(a)-1)], anchor)
    for pt in a:
        pt_ang = polar_angle(pt, anchor)
        if pt_ang < piv_angle:
            smaller.append(pt)
        elif pt_ang == piv_angle:
            equal.append(pt)
        else:
            larger.append(pt)

    return quicksort(smaller, anchor) + sorted(equal, key=lambda x: distance(x, anchor)) + quicksort(larger, anchor)


def create_points(ct, min=0, max=50):
    return [[randint(min, max), randint(min, max)] for _ in range(ct)]


def scatter_plot(coords, convex_hull=None):
    xs, ys = zip(*coords)
    plt.scatter(xs, ys)

    if not convex_hull:
        plt.show()
        return

    for i in range(1, len(convex_hull) + 1):
        if i == len(convex_hull):
            i = 0

        c0 = convex_hull[i-1]
        c1 = convex_hull[i]

        plt.plot((c0[0], c1[0]), (c0[1], c1[1]), 'r')
    plt.show()


def polar_angle(p0, p1):
    y_span = p0[1] - p1[1]
    x_span = p0[0] - p1[0]
    return atan2(y_span, x_span)


def distance(p0, p1):
    y_span = p0[1] - p1[1]
    x_span = p0[0] - p1[0]
    return y_span**2 + x_span**2


def det(p1, p2, p3):
    return (p2[0] - p1[0]) * (p3[1] - p1[1]) \
           - (p2[1] - p1[1]) * (p3[0] - p1[0])


if __name__ == '__main__':
    pts = create_points(150)
    print(pts)

    anc, hull = graham_scan(pts, False)
    scatter_plot(pts, hull)
    print(hull)
