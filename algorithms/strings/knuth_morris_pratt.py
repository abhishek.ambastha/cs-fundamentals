""" Knuth Morris Pratt Algorithm

Setup:
Let P be the pattern and T be the text.

Observations for building the prefix array:
--------------------------------------------------------------------------------------
1. Let prefix[i] be the largest integer smaller than i such that,
    P[0:prefix[i]+1] is a suffix of P[0:i+1] or

2. If P[0: prefix[i]+1] is a suffix of P[0:i+1], then
P[0:prefix[i]] is a suffix of  P[0: i]

3. All the suffix of P[0:i] can be obtained by recursively applying prefix to i,
i.e prefix[prefix[prefix[i]]] . I.e P[0:prefix[i]], P[0: prefix[prefix[i]]]
are all suffix of P[0:i]

Also Note that,
prefix[i] = prefix[prefix[prefix ...k-times... prefix[i-1]]]]] + 1
where k is the smallest integer that satisfies P[prefix... k-times [i-1]] + 1] = P[i]
if no such k, then make it -1

This gives a way to build the prefix array.

--------------------------------------------------------------------------------------

Now with the prefix array, to match,
set k = -1, and
keep matching prefix[k+1] to text[i],
in case of a mismatch, set k = prefix[k]

"""


def build_prefix_array(pattern):
    k, prefix = -2, [0] * len(pattern)

    for i in range(len(pattern)):
        while k >= -1 and pattern[k+1] != pattern[i]:
            k = -2 if k == -1 else prefix[k]

        k += 1
        prefix[i] = k

    return prefix


def match(text, pattern):
    prefix = build_prefix_array(pattern)

    k, result = -1, []
    for i in range(len(text)):
        while k >= -1 and pattern[k+1] != text[i]:
            k = -2 if k == -1 else prefix[k]

        k += 1

        if k == len(pattern) - 1:
            result.append(i-k)
            k = -2 if k == -1 else prefix[k]

    return result


def main():
    text = 'AABAACAADAABAABA'
    pattern = 'AABA'
    result = match(text, pattern)
    print(f'Matched {pattern}, {text}  at: {result}')

    text = 'abacaabaccabacabaabb'
    pattern = 'abacab'
    result = match(text, pattern)
    print(f'Matched {pattern}, {text}  at: {result}')


if __name__ == '__main__':
    main()
