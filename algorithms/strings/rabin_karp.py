"""

Match the hash first, then
1. Monte Carlo version: Probability of a spurious match is 1/Q (where Q is the prime
number used for taking the modulo

Explanation:
let t1 = d1 % Q and t2 = d2 % Q
now if t1 = t2 or t1-t2 = 0,
    => (d1-d2) % Q = 0

now there are two possibilities,
1. both d1 and d2 are 0 (will not be the case for our rolling hash function)
2. |d1-d2| = Q          (probability of |d1-d2| to be Q or a multiple of Q = 1/Q)

2. Las Vegas
Rolling Hash Function:
H = (s[0] * R^k-1 + s[1] * R^k-2 + ..... + s[k-1] * R^0) % Q

To compute the next hash value,
1. Subtract s[start_idx] * R^k-1 from old hash
2. Multiply R^k-1 to value obtained from 1
3. Add s[end_idx+1] to value obtained from 2

If hash is matched, match the string
"""


def int_val(char):
    return ord(char) - ord('a') + 1


def match(text, pattern):
    Q, R = 1021, 256
    phash, thash = 0, 0
    RM = R ** (len(pattern) - 1) % Q

    for i in range(len(pattern)):
        phash = (phash * R + int_val(pattern[i])) % Q
        thash = (thash * R + int_val(text[i])) % Q

    result = []
    for i in range(len(text) - len(pattern) + 1):
        if phash == thash:
            if pattern == text[i:len(pattern)+i]:
                result.append(i)

        if i + len(pattern) < len(text):
            thash = thash - int_val(text[i])*RM
            thash = thash * R + int_val(text[i+len(pattern)])
            thash = thash % Q

    return result


def main():
    text = 'AABAACAADAABAABA'
    pattern = 'AABA'
    result = match(text, pattern)
    print(f'Matched {pattern}, {text}  at: {result}')

    text = 'abacaabaccabacabaabb'
    pattern = 'abacab'
    result = match(text, pattern)
    print(f'Matched {pattern}, {text}  at: {result}')


if __name__ == '__main__':
    main()

