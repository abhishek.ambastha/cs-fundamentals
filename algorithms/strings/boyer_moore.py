"""
1. Match the pattern from right to left
2. *Mismatch Character Heuristic*
    - If mismatch happens and the character in text is
    not seen in pattern, shift by m (length of pattern)

    - If character is present in text, then shift by the
    m - min(k, j+1) as below. I.e if the mismatch character
    comes after the current pattern element,
    shift the pattern to right by 1

"""


def precompute_shift(pattern):
    last_seen = {}
    for i, c in enumerate(pattern):
        last_seen[c] = i

    return last_seen


def match(text, pattern):
    n, m = len(text), len(pattern)
    if m == 0: return 0

    shift = precompute_shift(pattern)

    i, k = m - 1, m - 1
    result = []
    while i < n and (i >= 0):
        if text[i] == pattern[k]:
            if k == 0:
                result.append(i)
                i, k = i + m, m - 1
            else:
                i, k = i - 1, k - 1
        else:
            j = shift.get(text[i], -1)
            i, k = i + m - min(k, j + 1), m - 1
    return result


def main():
    text = 'AABAACAADAABAABA'
    pattern = 'AABA'
    result = match(text, pattern)
    print(f'Matched {pattern}, {text}  at: {result}')

    text = 'abacaabaccabacabaabb'
    pattern = 'abacab'
    result = match(text, pattern)
    print(f'Matched {pattern}, {text}  at: {result}')


if __name__ == '__main__':
    main()




