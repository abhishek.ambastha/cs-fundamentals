def binary_search(arr, target_value):
    start, end = 0, len(arr) - 1

    while start <= end:
        mid = start + (end-start)//2
        if arr[mid] < target_value:
            start = mid + 1
        elif arr[mid] == target_value:
            return mid
        else:
            end = mid - 1

    return -1


if __name__ == '__main__':
    A = [5, 12, 34, 44, 98, 154]
    print(binary_search(A, 5))
    print(binary_search(A, 34))
    print(binary_search(A, 154))
#