class Elevator:
    NONE, UP, DOWN = range(3)

    def __init__(self, n_floor):
        self.nFloor = n_floor
        self.direction = Elevator.NONE
        self.goal = [False] * n_floor
        self.moving = False
        self.current_floor = 0
        self.pending_up = 0
        self.pending_down = 0
        self.max_target = -1
        self.min_target = n_floor

    def move(self):
        """
            1. If not moving, and a goal is set, update flag
            2.

        """
        if not self.moving:
            self.moving = self.direction != Elevator.NONE

        if self.direction == Elevator.UP:
            self.goal[self.current_floor] = False
            self.current_floor += 1
            self.pending_up -= 1

            if self.pending_up == 0:
                self.direction = Elevator.DOWN if self.pending_down > 0 else Elevator.NONE
                self.max_target = -1
            self.moving = False

        if self.direction == Elevator.DOWN:
            self.goal[self.current_floor] = False
            self.current_floor -= 1
            self.pending_down -= 1

            if self.pending_down == 0:
                self.direction = Elevator.UP if self.pending_up else Elevator.NONE
            self.moving = False

    def add_goal(self, goal_floor):
        """
        Update the states accordingly.
        :param goal_floor:
        :return:
        """
        if self.goal[goal_floor] or goal_floor == self.current_floor:
            return

        self.goal[goal_floor] = True
        if goal_floor > self.current_floor:
            self.pending_up += 1
        else:
            self.pending_down += 1

        if self.direction == Elevator.NONE:
            self.direction = Elevator.UP if goal_floor > self.current_floor else Elevator.DOWN


class ElevatorController:
    def __init__(self, n_elevators, n_floors):
        self.n_elevators = n_elevators
        self.n_floors = n_floors
        self.elevators = self.init_elevators()

    def init_elevators(self):
        """
            1. Create array of elevator objects with the floors
            2. If needed, add listeners
        """
        return [Elevator(self.n_floors) for _ in range(self.n_elevators)]

    def pickup(self, floor, direction):
        """
            1. Validate floor
            2. Calculate the closest elevator
            3. add goal floor to the elevator
        """
        pass

    def step(self):
        for elevator in self.elevators:
            elevator.move()
