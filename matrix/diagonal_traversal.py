def traverse_main_diag(N, M):
    for b in range(N+M-1):
        rmax = min(b+1, N)
        rstart = max(0, b-N+1)
        for r in reversed(range(rstart, rmax)):
            c = b - r
            print(f'({r}, {c})', ' ', end='')
        print('\n')


def traverse_other_diag(N, M):
    for b in range(-1*(M-1), N):
        rstart = max(0, b)
        rstop = min(N-1, b + M -1)
        for r in range(rstart, rstop+1):
            c = r - b
            print(f'({r}, {c})', ' ', end='')
        print('\n')


if __name__ == '__main__':
    # traverse_main_diag(3, 3)
    traverse_other_diag(4, 5)
