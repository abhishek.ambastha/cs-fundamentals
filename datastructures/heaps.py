class MinHeap:
    def __init__(self):
        self.vals = []

    @staticmethod
    def left(i):
        return 2*i + 1

    @staticmethod
    def right(i):
        return 2*i + 2

    @staticmethod
    def parent(i):
        return i // 2

    @property
    def size(self):
        return len(self.vals)

    def swim(self, i):
        while i > 0 and self.vals[i] < self.vals[self.parent(i)]:
            self.vals[i], self.vals[self.parent(i)] = \
                self.vals[self.parent(i)], self.vals[i]

            i = self.parent(i)

    def sink(self, i):
        while self.left(i) <= self.size:
            l, r = self.left(i), self.right(i)

            smallest = i
            if l < self.size and self.vals[smallest] > self.vals[l]:
                smallest = l

            if r < self.size and self.vals[smallest] > self.vals[r]:
                smallest = r

            if smallest != i:
                self.vals[i], self.vals[smallest] = \
                    self.vals[smallest], self.vals[i]
                i = smallest
            else:
                break

    def insert(self, element):
        self.vals.append(element)
        self.swim(len(self.vals)-1)

    def delete_min(self):
        val = self.vals[0]
        self.vals[0], self.vals[-1] = self.vals[-1], self.vals[0]
        self.vals = self.vals[0:-1]
        self.sink(0)
        return val

    def heapify(self):
        pass


if __name__ == '__main__':
    hh = MinHeap()
    vals = [9, 4, 5, 4, 7, 8, 8, 11]
    for v in vals:
        hh.insert(v)

    import heapq
    heapq.heapify(vals)

    for _ in range(len(vals)):
        print(heapq.heappop(vals), hh.delete_min())
