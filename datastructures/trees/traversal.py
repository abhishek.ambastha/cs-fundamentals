import collections
from collections import deque

import bintrees

from datastructures.trees import basic
from datastructures.trees.basic import dummy_tree


def level_order(tree):
    if not tree:
        return []

    q = deque([[tree]])
    result = []
    while len(q):
        curr_nodes = q.popleft()
        temp, next_level = [], []
        for node in curr_nodes:
            temp.append(node)

            if node.left:
                next_level.append(node.left)

            if node.right:
                next_level.append(node.right)

        if len(next_level) > 0:
            q.append(next_level)

        result.append(temp)

    return result


def level_order_count(tree):
    if not tree:
        return []

    q = deque([tree])
    result = []
    while q:
        count = len(q)
        temp = []
        while count:
            node = q.popleft()
            temp.append(node)

            if node.left:
                q.append(node.left)

            if node.right:
                q.append(node.right)

            count -= 1
        result.append(temp)

    return result


def vertical_order(tree):
    node_and_level = collections.namedtuple('node_and_level', ('node', 'level'))
    result = bintrees.RBTree()

    q = deque([node_and_level(tree, 0)])
    while q:
        node_level = q.popleft()
        node, vlevel = node_level.node, node_level.level

        if vlevel not in result:
            result[vlevel] = list()
        result[vlevel].append(node)

        if node.left:
            q.append(node_and_level(node.left, vlevel - 1))

        if node.right:
            q.append(node_and_level(node.right, vlevel + 1))

    return result


def top_view(tree):
    vertical = vertical_order(tree)
    result = []
    for k in vertical.keys():
        result.append(vertical[k][0])

    return result


def bottom_view(tree):
    vertical = vertical_order(tree)
    result = []
    for k in vertical.keys():
        result.append(vertical[k][-1])

    return result


def diagonal_view(tree):
    node_and_level = collections.namedtuple('node_and_level', ('node', 'level'))
    q = deque([node_and_level(tree, 0)])
    result = bintrees.RBTree()

    while q:
        curr = q.popleft()
        node, dlevel = curr.node, curr.level

        if dlevel not in result:
            result[dlevel] = list()
        result[dlevel].append(node)

        if node.left:
            q.append(node_and_level(node.left, dlevel + 1))

        if node.right:
            q.append(node_and_level(node.right, dlevel))

    return result


def spiral_traversal(tree):
    if not tree:
        return []

    result = []
    st1, st2 = deque(), deque()
    st1.append(tree)

    while st1 or st2:
        temp = []
        while st1:
            curr = st1.pop()
            temp.append(curr)

            if curr.left:
                st2.append(curr.left)

            if curr.right:
                st2.append(curr.right)
        result.append(temp)

        temp = []
        while st2:
            curr = st2.pop()
            temp.append(curr)

            if curr.right:
                st1.append(curr.right)

            if curr.left:
                st1.append(curr.left)
        result.append(temp)

    return result


def boundary(tree):
    result = []
    if not tree:
        return result

    result.append(tree)
    left_boundary(tree.left, True, result)
    right_boundary(tree.right, True, result)

    return result


def left_boundary(node, is_boundary, result):
    if not node:
        return

    if is_boundary or is_leaf(node):
        result.append(node)

    left_boundary(node.left, is_boundary, result)
    left_boundary(node.right, is_boundary and not node.left, result)


def right_boundary(node, is_boundary, result):
    if not node:
        return

    right_boundary(node.left, is_boundary and not node.right, result)
    right_boundary(node.right, is_boundary, result)

    if is_boundary or is_leaf(node):
        result.append(node)


def is_leaf(node):
    if not node:
        return False

    return not node.left and not node.right


def test():
    tree = dummy_tree()

    basic.test()

    print('level order traversal')
    lorder = level_order(tree.root)
    print(lorder)

    llorder = level_order_count(tree.root)
    print(llorder)
    print('***********************************')

    print('\nVertical order')
    vertical = vertical_order(tree.root)
    print(vertical)
    # for vl in vertical.keys():
    #     print(vl, vertical[vl])
    print('***********************************')

    print('\nTop view')
    top = top_view(tree.root)
    print(top)
    print('***********************************')

    print('\nBottom view')
    bottom = bottom_view(tree.root)
    print(bottom)
    print('***********************************')

    print('\nDiagonal view')
    diag = diagonal_view(tree.root)
    print(diag)
    # for k in diag.keys():
    #     print(k, diag[k])
    print('***********************************')

    print('\nExterior of binary tree')
    exterior = boundary(tree.root)
    print(exterior)
    print('***********************************')

    print('\nSpiral order of binary tree')
    spiral = spiral_traversal(tree.root)
    print(spiral)
    print('***********************************')


if __name__ == '__main__':
    test()
