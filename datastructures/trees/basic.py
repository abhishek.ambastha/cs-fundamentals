import pydot
import tempfile, PIL.Image


class Node:
    def __init__(self, value):
        self.value = value
        self.left = None
        self.right = None

    def __repr__(self):
        return str(self.value)


class BinaryTree:
    def __init__(self, root):
        self.root = Node(root)


def dummy_tree():
    tree = BinaryTree(1)
    tree.root.left = Node(2)
    tree.root.right = Node(3)
    tree.root.left.left = Node(4)
    tree.root.left.right = Node(5)
    tree.root.right.left = Node(6)
    tree.root.right.right = Node(7)
    tree.root.right.right.right = Node(8)

    return tree


def test():

    tree = dummy_tree()

    graph = pydot.Dot(graph_type='digraph')
    inorder_tree(tree.root, graph)

    fout = tempfile.NamedTemporaryFile(suffix=".png")
    graph.write(fout.name, format="png")
    PIL.Image.open(fout.name).show()


def inorder_tree(node, graph):
    if not node:
        return

    left_child = inorder_tree(node.left, graph)

    curr_node = pydot.Node(node.value)
    graph.add_node(curr_node)

    right_child = inorder_tree(node.right, graph)

    if left_child:
        graph.add_edge(pydot.Edge(curr_node, left_child))
    # else:
    #     graph.add_edge(pydot.Edge(curr_node, pydot.Node(str(node.value)+'$', style='filled', color='red'), weight=0,
    #                               style='invisible', color="white", arrowhead="none"))

    if right_child:
        graph.add_edge(pydot.Edge(curr_node, right_child))
    # else:
    #     graph.add_edge(pydot.Edge(curr_node, pydot.Node(str(node.value)+'$', style='filled', color='red'), weight=0,
    #                               style='invisible', color="white", arrowhead="none"))

    return curr_node


if __name__ == '__main__':
    test()

