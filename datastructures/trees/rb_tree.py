"""
Python implementation of left-leaning red-black tree from
Robert Sedgeweck's lecture.

Note: The RBTree has a direct correspondence with 2-3 trees,
where the red-links (link between a red-node and a black-node
can be considered as the glue between 2 2-nodes to make a 3-node!
(left leaning in this impl)

Properties:
----------
1. No node will have 2 red links connected to it.
2. Every path from root to null link has the same
number of <em>black links</em>
3. Red links lean left.



"""


class Node:
    RED, BLACK = True, False

    def __init__(self, key, value=None, color=RED):
        self.key = key
        self.val = value
        self.color = color

        self.left = None
        self.right = None


class RBTree:
    def __init__(self, root):
        self.root = root

    def is_red(self, node):
        if not node:
            return False

        return node.color == Node.RED

    def put(self, key, value):
        self.root = self._put(self.root, key, value)
        self.root.color = Node.BLACK

    def _put(self, node, key, value):
        """
        Always insert a red-node.
        """
        if not node:
            return Node(key, value, Node.RED)

        if key < node.key:
            node.left = self._put(node.left, key, value)
        elif key > node.key:
            node.right = self._put(node.right, key, value)
        else:
            node.val = value

        """
        <pre>
        Balancing strategy: 

                (c)                             (c)
               //         (left-rotate)        //      
              (a)             ====>           (b)      
             / \\                            // \
                (b)                         (a)



              (c)                                (b)
             //           (right-rotate)        // \\
            (b)              ====>            (a)   (c)
           // \
        (a)


                                                  ||
            (b)           (flip-color)            (b)
           // \\             ====>               /   \
         (a)   (c)                             (a)   (c)

        </pre>
        """

        if self.is_red(node.right) and not self.is_red(node.left):
            node = self._rotate_left(node)

        if self.is_red(node.left) and self.is_red(node.left.left):
            node = self._rotate_right(node)

        if self.is_red(node.left) and self.is_red(node.right):
            self._flip_color(node)

        return node

    def _rotate_left(self, h):
        """
        Counter-clock wise rotation

        <pre>
            Before transformation:

                        |
                        E    (h)
                     /   \\
                  (<E)     S  (x)
                         /  \
                    (>E <S)  (>S)

            After transformation:

                         |
                         S  (x)
                       // \
                (h)   E   greater than S (>S)
                    /   \
                 (<E)  (>E <S)

        </pre>

        :param h: the node at which rotation will be performed
        :return: None
        """
        x = h.right
        h.right = x.left
        x.left = h
        x.color = h.color  # important, could be 2 reds
        h.color = Node.RED

        return x

    def _rotate_right(self, h):
        """
        Clock-wise rotation

        <pre>
            Before transformation

                      |
                      S  (h)
                    // \
            (x)    E    (>S)
                  / \
               (<E) (>E <S)


            After transformation:

                       |
                       E   (x)
                     /  \\
                  (<E)   S (h)
                        /  \
                   (>E <S)  (>S)

        </pre>

        :param h: the node at which rotation will be performed
        :return: None
        """
        x = h.left
        h.left = x.right
        x.right = h
        x.color = h.color
        h.color = Node.RED

        return x

    def _flip_color(self, h):
        """
        if both children have red color, we will flip their colors
        to black, and the root to be red

        :param h: root node of subtree
        :return:
        """
        h.color = Node.RED
        h.left.color = Node.BLACK
        h.right.color = Node.BLACK

    def get(self, key):
        node = self.root
        while node:
            if key < node.key:
                node = node.left
            elif key > node.key:
                node = node.right
            else:
                return node
        return None

    def delete(self, key):
        pass


def test():
    pass


if __name__ == '__main__':
    test()
