from datastructures.trees.basic import dummy_tree


def postorder(node):
    if node:
        postorder(node.left)
        postorder(node.right)
        print(node.value, ' ', end='')


def postorder_two_stack(node):
    st1, st2 = [], []
    st1.append(node)

    while st1:
        curr = st1.pop()
        if curr.left:
            st1.append(curr.left)

        if curr.right:
            st1.append(curr.right)

        st2.append(curr)

    while st2:
        print(st2.pop().value, ' ', end='')


def postorder_single_stack(node):
    st, current = [], node

    while current or len(st) > 0:
        if current:
            st.append(current)
            current = current.left

        else:
            temp = st[-1].right
            if not temp:
                temp = st.pop()
                print(temp.value, ' ', end='')

                while st and temp == st[-1].right:
                    temp = st.pop()
                    print(temp.value, ' ', end='')
            else:
                current = temp


def test():
    tree = dummy_tree()
    postorder(tree.root)
    print('\n *********')
    postorder_two_stack(tree.root)

    print('\n *********** ')
    postorder_single_stack(tree.root)


if __name__ == '__main__':
    test()
