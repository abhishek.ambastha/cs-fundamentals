class TreeNode:
    def __init__(self, key, value=None):
        self.key = key
        self.value = value
        self.left = None
        self.right = None


class BinarySearchTree:
    def __init__(self, root):
        self.root = root

    def put(self, key, value):
        self.root = self._put(self.root, key, value)

    def _put(self, node, key, value):
        if not node:
            return TreeNode(key, value)

        if key < node.key:
            node.left = self._put(node.left, key, value)
        elif key > node.key:
            node.right = self._put(node.right, key, value)
        else:
            node.value = value

        return node

    def get(self, key):
        node = self.root
        while node:
            if key < node.key:
                node = node.left
            elif key > node.key:
                node = node.right
            else:
                return node
        return None

    def delete(self, key):
        self.root = self._delete(self.root, key)

    def _delete(self, node, key):
        if not node:
            return None

        if key < node.key:
            node.left = self._delete(node.left, key)
        elif key > node.key:
            node.right = self._delete(node.right, key)
        else:
            if not node.right:
                return node.left
            if not node.left:
                return node.right

            # both children, Hibbard deletion
            temp = node                                # save a link to the node to be deleted (del. node)
            node = self._min(node.right)               # find the successor, which will replace the del. node
            node.right = self._delete_min(temp.right)  # deletes the successor(node) from original position
            node.left = temp.left                      # prev line and this replaces the node to be deleted

        return node

    def _min(self, node):
        while node and node.left:
            node = node.left

        return node

    def delete_min(self):
        self.root = self._delete_min(self.root)

    def _delete_min(self, node):
        if not node.left:
            return node.right
        node.left = self._delete_min(node.left)
        return node

    def floor(self, key):
        node = self._floor(self.root, key)
        if node:
            return node.key

        return None

    def _floor(self, node, key):
        if not node:
            return None

        if node.key == key:
            return node

        if node.key > key:
            return self._floor(node.left, key)

        temp = self._floor(node.right, key)
        if not temp:
            return node
        return temp

    def ceil(self, key):
        node = self._ceil(self.root, key)
        if node:
            return node.key

        return None

    def _ceil(self, node, key):
        if not node:
            return None

        if node.key == key:
            return node

        if node.key < key:
            return self._ceil(node.right, key)

        temp = self._ceil(node.left, key)
        if not temp:
            return node
        return temp

    def inorder(self):
        self._inorder(self.root)
        print('\n')

    def _inorder(self, node):
        if node:
            self._inorder(node.left)
            print(node.key, ' ', end='')
            self._inorder(node.right)

    def __iter__(self):
        pass


def test():
    btree = BinarySearchTree(TreeNode('D'))
    btree.put('E', None)
    btree.put('A', None)
    btree.put('C', None)

    btree.inorder()
    print(btree.floor('B'))
    print(btree.ceil('B'))

    btree.delete('D')
    btree.inorder()


if __name__ == '__main__':
    test()
