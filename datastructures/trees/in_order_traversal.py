from datastructures.trees.basic import dummy_tree


def inorder(node):
    if node:
        inorder(node.left)
        print(node.value, ' ', end='')
        inorder(node.right)


def inorder_stack(node):
    print('Inorder with stack')
    st = []
    st.append(node)
    visited = {}

    while st:
        curr = st.pop()
        if curr in visited:
            print(curr.value, ' ', end='')
        else:
            visited[curr] = True
            if curr.right:
                st.append(curr.right)

            st.append(curr)

            if curr.left:
                st.append(curr.left)

    # while True:
    #     if node:
    #         # keep adding on stack till no further left child
    #         st.append(node)
    #         node = node.left
    #
    #     else:
    #         if st:
    #             # pop, and print node
    #             curr = st.pop()
    #             print(curr.value, ' ', end='')
    #
    #             # process right child
    #             node = curr.right
    #         else:
    #             break


def inorder_without_stack(node):
    """
    Morris Traversal
    :return:
    """
    current = node
    while current:
        if not current.left:
            # there is no left child, so visit and and goto right child
            print(current.value, ' ', end='')
            current = current.right
        else:
            # left is not null, so find the node which will
            # come before the current node == inorder-pred.
            # or the rightmost node in left subtree.
            pred = current.left
            while pred.right and pred.right != current:
                pred = pred.right

            if not pred.right:
                # if no right child of pred, right child
                # to be the current node.
                # also move current to the left subtree
                pred.right = current
                current = current.left
            else:
                # pred was visited before and link to current
                # node was created. So delete this link and
                # visit the current node. and move current to right
                # child
                pred.right = None
                print(current.value, ' ', end='')
                current = current.right


if __name__ == '__main__':
    tree = dummy_tree()
    inorder(tree.root)
    print('\n *********** ')
    inorder_stack(tree.root)
    print('\n *********** ')
    inorder_without_stack(tree.root)
