"""
1. Each rule, R, defined in the grammar, becomes a method with the same name,
and references to that rule become a method call: R(). The body of the method
follows the flow of the body of the rule using the very same guidelines.

2. Alternatives (a1 | a2 | aN) become an if-elif-else statement

3. An optional grouping (…)* becomes a while statement that can loop over zero
or more times

4. Each token reference T becomes a call to the method eat: eat(T).
The way the eat method works is that it consumes the token T if it
matches the current lookahead token, then it gets a new token from
the lexer and assigns that token to the current_token internal variable.

Example:
    expr: factor((PLUS | MINUS | MULTIPLY | DIVIDE) factor)*
    factor: INTEGER

Answer the following:
What is a context-free grammar (grammar)?
How many rules / productions does the grammar have?
What is a terminal? (Identify all terminals in the picture)
What is a non-terminal? (Identify all non-terminals in the picture)
What is a head of a rule? (Identify all heads / left-hand sides in the picture)
What is a body of the rule? (Identify all bodies / right-hand sides in the picture)
What is the start symbol of a grammar?

"""


class Token:
    PLUS, MINUS, MULTIPLY, DIVIDE = 'PLUS', 'MINUS', 'MULTIPLY', 'DIVIDE'
    INTEGER, EOF = 'INTEGER', 'EOF'

    def __init__(self, type, value):
        self.type = type
        self.value = value

    def __str__(self):
        return f'Token({self.type}, {self.value})'


class Lexer:
    def __init__(self, text):
        self.text = text
        self.pos = 0
        self.current_char = self.text[self.pos]

    def advance(self):
        self.pos += 1
        if self.pos < len(self.text):
            self.current_char = self.text[self.pos]
        else:
            self.current_char = None

    def skip_whitespace(self):
        while self.current_char is not None and self.current_char.isspace():
            self.advance()

    def integer(self):
        result = ''
        while self.current_char is not None and self.current_char.isdigit():
            result += self.current_char
            self.advance()

        return int(result)

    def get_next_token(self):
        while self.current_char is not None:
            if self.current_char.isspace():
                self.skip_whitespace()
                continue

            if self.current_char.isdigit():
                return Token(Token.INTEGER, self.integer())

            if self.current_char == '+':
                self.advance()
                return Token(Token.PLUS, '+')

            if self.current_char == '-':
                self.advance()
                return Token(Token.MINUS, '-')

            if self.current_char == '*':
                self.advance()
                return Token(Token.MULTIPLY, '*')

            if self.current_char == '/':
                self.advance()
                return Token(Token.DIVIDE, '/')

        return Token(Token.EOF, None)


class Interpreter:
    def __init__(self, lexer):
        self.lexer = lexer
        self.current_token = lexer.get_next_token()

    def eat(self, token_type):
        if self.current_token.type == token_type:
            self.current_token = self.lexer.get_next_token()
        else:
            raise Exception('Invalid syntax')

    def expr(self):
        result = self.factor()

        while self.current_token.type in (Token.PLUS, Token.MINUS, Token.MULTIPLY, Token.DIVIDE):
            if self.current_token.type == Token.PLUS:
                self.eat(Token.PLUS)
                result += self.factor()
            elif self.current_token.type == Token.MINUS:
                self.eat(Token.MINUS)
                result -= self.factor()
            elif self.current_token.type == Token.MULTIPLY:
                self.eat(Token.MULTIPLY)
                result *= self.factor()
            elif self.current_token.type == Token.DIVIDE:
                self.eat(Token.DIVIDE)
                result /= self.factor()
            else:
                raise Exception('Invalid syntax')

        return result

    def factor(self):
        token = self.current_token
        self.eat(Token.INTEGER)
        return token.value


def main():
    while True:
        try:
            text = input('calc> ')
        except EOFError:
            break

        lexer = Lexer(text)
        interpreter = Interpreter(lexer)
        result = interpreter.expr()
        print(result)


if __name__ == '__main__':
    main()
