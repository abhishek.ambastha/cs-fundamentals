"""
What is an interpreter?
What is a compiler?
What’s the difference between an interpreter and a compiler?
What is a token?
What is the name of the process that breaks input apart into tokens?
What is the part of the interpreter that does lexical analysis called?
What are the other common names for that part of an interpreter or a compiler?
What is a lexeme?
What is the name of the process that finds the structure in the stream of tokens,
or put differently, what is the name of the process that recognizes a certain phrase in that stream of tokens?
What is the name of the part of the interpreter (compiler) that does parsing?
"""


class Token:
    INTEGER, OP, EOF = 'INTEGER', 'PLUS', 'EOF'

    def __init__(self, type, value):
        self.type = type
        self.value = value

    def __str__(self):
        return f'TOKEN({self.type}, {self.value})'


class Interpreter:
    def __init__(self, text):
        self.text = text
        self.pos = 0
        self.current_token = None

    def get_next_token(self):
        text = self.text

        # skip whitespaces
        while self.pos < len(text) and text[self.pos] == ' ':
            self.pos += 1

        # check for EOF
        if self.pos > len(text) - 1:
            return Token(Token.EOF, None)

        # get string for token
        curr_char = text[self.pos]
        self.pos += 1

        # look-ahead and append if applicable
        while curr_char.isdigit() and self.pos < len(text) and text[self.pos].isdigit():
            curr_char += text[self.pos]
            self.pos += 1

        # form the token
        token = None
        if curr_char.isdigit():
            token = Token(Token.INTEGER, int(curr_char))
        elif curr_char == '+' or curr_char == '-':
            token = Token(Token.OP, curr_char)

        return token

    def eat(self, token_type):
        if self.current_token.type == token_type:
            self.current_token = self.get_next_token()
        else:
            raise ValueError

    def expr(self):
        self.current_token = self.get_next_token()

        left = self.current_token
        self.eat(Token.INTEGER)

        op = self.current_token
        self.eat(Token.OP)

        right = self.current_token
        self.eat(Token.INTEGER)

        result = None
        if op.value == '+':
            result = left.value + right.value
        elif op.value == '-':
            result = left.value - right.value

        return result


def main():
    while True:
        try:
            text = input('calc>')
        except EOFError:
            break

        if not text:
            continue
        interpreter = Interpreter(text)
        result = interpreter.expr()
        print(result)


if __name__ == '__main__':
    main()
