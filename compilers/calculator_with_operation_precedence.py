"""
1. For each level of precedence define a non-terminal. The body of a production
for the non-terminal should contain arithmetic operators from that level and
non-terminals for the next higher level of precedence.
2. Create an additional non-terminal factor for basic units of expression, in our case,
integers. The general rule is that if you have N levels of precedence, you will need N + 1
non-terminals in total: one non-terminal for each level plus one non-terminal for basic units
of expression.

Grammar:

expr: term((PLUS | MINUS) term)
term: factor((MULTIPLY | DIVIDE) factor)
factor: INTEGER

"""


class Token:
    PLUS, MINUS, MULTIPLY, DIVIDE = 'PLUS', 'MINUS', 'MULTIPLY', 'DIVIDE'
    INTEGER, EOF = 'INTEGER', 'EOF'

    def __init__(self, type, value):
        self.type = type
        self.value = value

    def __str__(self):
        return f'Token({self.type}, {self.value})'


class Lexer:
    def __init__(self, text):
        self.text = text
        self.pos = 0
        self.current_char = self.text[self.pos]

    def advance(self):
        self.pos += 1
        if self.pos < len(self.text):
            self.current_char = self.text[self.pos]
        else:
            self.current_char = None

    def skip_whitespace(self):
        while self.current_char is not None and self.current_char.isspace():
            self.advance()

    def integer(self):
        result = ''
        while self.current_char is not None and self.current_char.isdigit():
            result += self.current_char
            self.advance()

        return int(result)

    def get_next_token(self):
        while self.current_char is not None:
            if self.current_char.isspace():
                self.skip_whitespace()
                continue

            if self.current_char.isdigit():
                return Token(Token.INTEGER, self.integer())

            if self.current_char == '+':
                self.advance()
                return Token(Token.PLUS, '+')

            if self.current_char == '-':
                self.advance()
                return Token(Token.MINUS, '-')

            if self.current_char == '*':
                self.advance()
                return Token(Token.MULTIPLY, '*')

            if self.current_char == '/':
                self.advance()
                return Token(Token.DIVIDE, '/')

            raise Exception('Invalid syntax')

        return Token(Token.EOF, None)


class Interpreter:
    def __init__(self, lexer):
        self.lexer = lexer
        self.current_token = lexer.get_next_token()

    def eat(self, token_type):
        if self.current_token.type == token_type:
            self.current_token = self.lexer.get_next_token()
        else:
            raise Exception('Invalid syntax')

    def factor(self):
        token = self.current_token
        self.eat(Token.INTEGER)
        return token.value

    def term(self):
        result = self.factor()

        while self.current_token.type in (Token.MULTIPLY, Token.DIVIDE):
            if self.current_token.type == Token.MULTIPLY:
                self.eat(Token.MULTIPLY)
                result *= self.factor()
            elif self.current_token.type == Token.DIVIDE:
                self.eat(Token.DIVIDE)
                result /= self.factor()
            else:
                raise Exception('Invalid syntax')

        return result

    def expr(self):
        result = self.term()

        while self.current_token.type in (Token.PLUS, Token.MINUS):
            if self.current_token.type == Token.PLUS:
                self.eat(Token.PLUS)
                result += self.term()
            elif self.current_token.type == Token.MINUS:
                self.eat(Token.MINUS)
                result -= self.term()
            else:
                raise Exception('Invalid syntax')

        return result


def main():
    while True:
        try:
            text = input('calc> ')
        except EOFError:
            break

        lexer = Lexer(text)
        interpreter = Interpreter(lexer)
        print(interpreter.expr())


if __name__ == '__main__':
    main()
