# CS Fundamentals

This repository contains code for exploring basic computer science concepts like datastructures, algorithms, compilers and operation system, mainly for educational purposes.
Most of the implementations are optimized for readibility and simplification rather than performance, while keeping asymptotic bounds to the best possible at the same time.
